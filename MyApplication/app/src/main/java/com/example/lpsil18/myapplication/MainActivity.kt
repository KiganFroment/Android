package com.example.lpsil18.myapplication

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button

class MainActivity : AppCompatActivity() {

    lateinit var btnSettings : Button

    private var username : String by DelegatesExt.preference(this, SettingsActivity.PREFERENCE_USER, SettingsActivity.DEFAULT_USER)
    private var sonore : Boolean by DelegatesExt.preference(this, SettingsActivity.PREF_SON, SettingsActivity.DEFAULT_SON)
    private var vibration : Boolean by DelegatesExt.preference(this, SettingsActivity.PREF_VIB, SettingsActivity.DEFAULT_VIB)
    private var vitesse : Int by DelegatesExt.preference(this, SettingsActivity.PREFERENCE_VITDEPL, SettingsActivity.DEFAULT_VITDEPL)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar!!.setTitle(R.string.main_title)

        Log.i("Username", "Username : " + username)
        Log.i("Son", "Sound enabled : " + sonore)
        Log.i("Vibration", "Sound enabled : " + vibration)
        Log.i("Vitesse", "Move speed : " + vitesse)


        btnSettings = findViewById(R.id.btnSettings) as Button
        btnSettings.setOnClickListener {
            startActivity(Intent(this, SettingsActivity::class.java))
        }
    }
}
