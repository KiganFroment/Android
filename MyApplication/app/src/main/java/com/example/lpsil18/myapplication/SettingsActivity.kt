package com.example.lpsil18.myapplication

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.widget.Button
import android.widget.EditText
import android.widget.SeekBar
import android.widget.Switch

class SettingsActivity : AppCompatActivity() {

    companion object {
        val PREFERENCE_USER = "username"
        val DEFAULT_USER = "Guest"

        val PREFERENCE_SON = "activeSon"
        val DEFAULT_SON = true

        val PREFERENCE_VIBRATION = "activeVib"
        val DEFAULT_VIB = true

        val PREFERENCE_VITDEPL = "vitesseDepl"
        val DEFAULT_VITDEPL = 10
    }

    private var username : String by DelegatesExt.preference(this, PREFERENCE_USER, DEFAULT_USER)
    private var sonore : Boolean by DelegatesExt.preference(this, PREFERENCE_SON, DEFAULT_SON)
    private var vibration : Boolean by DelegatesExt.preference(this, PREFERENCE_VIBRATION, DEFAULT_VIB)
    private var vitesse : Int by DelegatesExt.preference(this, PREFERENCE_VITDEPL, DEFAULT_VITDEPL)

    private lateinit var editName : EditText
    private lateinit var enableSon : Switch
    private lateinit var enableVib : Switch
    private lateinit var vitesseBar: SeekBar
    private lateinit var btnHome : Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        supportActionBar!!.setTitle(R.string.settings_title)

        editName = findViewById(R.id.nameEdit) as EditText
        enableSon = findViewById(R.id.prefSon) as Switch
        enableVib = findViewById(R.id.prefVib) as Switch
        vitesseBar = findViewById(R.id.vitesseBar) as SeekBar
        btnHome = findViewById(R.id.button) as Button

        editName.text = Editable.Factory.getInstance().newEditable(username)
        enableSon.isChecked = sonore
        enableVib.isChecked = vibration
        vitesseBar.progress = vitesse
        btnHome.setOnClickListener {
            save()
            startActivity(Intent(this, MainActivity::class.java))
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        save()
    }

    fun save(){
        username = editName.text.toString()
        sonore = enableSon.isEnabled
        vibration = enableVib.isEnabled
        vitesse = vitesseBar.progress
    }
}
